VAR gate1_is_open = 0
VAR gate2_is_open = 0

=== NPC_1 ===
// most basic test case: a single line
Hello, World!
-> END

=== NPC_2 ===
// multi line example
Multi line example
Second line
Third line
-> END


=== NPC_3 ===
// a use case with options. NPC will open gate1
Beyond this gate is the dark forest.
{	gate1_is_open > 0 : Good luck -> END }
{	gate1_is_open == 0 : Are your ready to proceed?	}
*	[Yes]
	Very well then, please go ahead. <>
	~ gate1_is_open = 1
	-> END
+ 	[No]
	Come back when you are ready. -> END

//=== gate_opened ===
//~ gate1_is_open = 1
//-> END

=== open_gate_2 ===
{ gate2_is_open > 0 : Nothing happened -> END }
Click! <>
~ gate2_is_open = 1
-> END
﻿using UnityEngine;
using Ink.Runtime;
using System.Collections;

public class DialogueManager : MonoBehaviour {

	public static DialogueManager instance;
	public UI_ShowDialogueText ui_ShowDialogueText;

	public TextAsset inkAsset;

	Story _inkStory;

	bool dialogueActive = false;
	bool dialogueShouldClose = false;

	void Awake() {
		instance = this;
		_inkStory = new Story(inkAsset.text);
	}

	// Use this for initialization
	void Start () {
		
	}

	public void RegisterObserver(string variable, Story.VariableObserver observer ) {
		_inkStory.ObserveVariable(variable, observer);
	}

	public void ShowDialogue(string dialogueKey) {
		// in case of an already active dialogue, this needs to finish first before we accept a new dialogue.
		if (dialogueActive) {
			Debug.Log("There is still a dialogue active.");
			return;
		}

		// we use this extra variable to make sure that the final time we click, the dialoguewindow is closed
		// instead of directly opening a new window.
		if (dialogueShouldClose) {
			dialogueShouldClose = false;
			return;
		}

		Debug.Log(dialogueKey);
		dialogueActive = true;
		dialogueShouldClose = false;
		_inkStory.ChoosePathString(dialogueKey);

		ShowCurrentStoryLine();

	}

	void ShowCurrentStoryLine() {
		// dialogue was closed, is there more to show?
		if (_inkStory.canContinue) {
			ui_ShowDialogueText.Show(_inkStory.Continue(), ShowCurrentStoryLine);
		} else if (_inkStory.currentChoices.Count > 0 ) { 		// perhaps there are still options to continue?
			string[] options = new string[_inkStory.currentChoices.Count];
			for (int i=0; i < options.Length; i++) {
				options[i] = _inkStory.currentChoices[i].text;
			}

			ui_ShowDialogueText.ShowResponsOptions(options, SelectedRespons);
		} else {
			// the dialogue is really finished.
			Debug.Log("Story complete. ");
			dialogueActive = false;
			dialogueShouldClose = true;
		}


	}

	public void SelectedRespons(int responsID) {
		Debug.Log("responsID: "+responsID);
		_inkStory.ChooseChoiceIndex(responsID);
		ShowCurrentStoryLine();
	}
}

﻿using UnityEngine;
using System.Collections;

public class DoorController : MonoBehaviour {

	public string variableToObserve = "gate_1_open";

	// Use this for initialization
	void Start () {
		DialogueManager.instance.RegisterObserver(variableToObserve, (string varName, object newValue) => {
			GateOpened(varName, (int)newValue);
		});
	}
	
	void GateOpened(string varName, int value) {
		if (varName == variableToObserve && value > 0 ){
			StartCoroutine("OpenGateCR");
		}
	}

	IEnumerator OpenGateCR() {
		Vector3 originalPosition = transform.position;
		Vector3 newPosition = transform.position - Vector3.up;


		float t = 0;
		while (t < 1) {

			transform.position = Vector3.Lerp(originalPosition, newPosition, t);
			yield return null;
			t += Time.deltaTime;
		}
	}
}

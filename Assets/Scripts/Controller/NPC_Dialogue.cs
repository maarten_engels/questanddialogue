﻿using UnityEngine;
using System.Collections;

public class NPC_Dialogue : MonoBehaviour {

	public string dialogueKey;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void StartDialogue() {
		DialogueManager.instance.ShowDialogue(dialogueKey);
	}
}

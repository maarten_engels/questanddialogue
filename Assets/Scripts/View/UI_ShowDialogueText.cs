﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class UI_ShowDialogueText : MonoBehaviour {

	public GameObject dialoguePanel;
	public Text text;
	public Button responsOptionPrefab;
	public bool typeWriter = false;
	public float charactersPerSecond = 60;

	bool isShowing = true;
	Action OnActionComplete;
	List<Button> responsButtons;


	// Use this for initialization
	void Start () {
		responsButtons = new List<Button>();
		Hide();
	}
	
	// Update is called once per frame
	void Update () {
		if (isShowing && Input.GetButtonDown("Fire1")) {
			Hide();
		}
	}

	void Hide() {
		dialoguePanel.SetActive(false);
		isShowing = false;

		if (OnActionComplete != null) {
			OnActionComplete();
		}
	}


	public void ShowResponsOptions(string[] options, Action<int> responsID) {
		
		// TODO: add interaction for responses
		dialoguePanel.SetActive(true);
		RectTransform rt = responsOptionPrefab.GetComponent<RectTransform>();
		float yPos = rt.anchoredPosition.y;
		float xPos = rt.anchoredPosition.x;
		for (int i=0 ; i < options.Length; i++) {
			Button b = Instantiate(responsOptionPrefab, dialoguePanel.transform) as Button;
			b.GetComponent<RectTransform>().anchoredPosition = new Vector2(xPos, yPos - (i * rt.sizeDelta.y));
			b.GetComponentInChildren<Text>().text = options[i];
			b.gameObject.SetActive(true);
			int new_i = i;
			b.onClick.AddListener(() => responsID(new_i));
			responsButtons.Add(b);
		}
	}

	void ClearResponses() {
		foreach (Button b in responsButtons) {
			Destroy(b.gameObject);
		}
		responsButtons.Clear();
	}

	public void Show(string text, Action showCompleteAction) {
		if (isShowing) {
			return;
		}

		ClearResponses();
		this.OnActionComplete = showCompleteAction;
		dialoguePanel.SetActive(true);
		if (typeWriter) {
			StartCoroutine(TypeWrite(text));
		} else {
			this.text.text = text;	
			isShowing = true;
		}
	}

	IEnumerator TypeWrite(string text) {
		Char[] charArray = text.ToCharArray();

		string showString = "";

		int charactersPerFrame = Mathf.Max(Mathf.FloorToInt(charactersPerSecond * Time.deltaTime), 1);

		int i = 0;
		while (i < text.Length) {
			for (int j = 0; j < charactersPerFrame; j++ ) {
				if (i + j < charArray.Length) {
					showString += charArray[i+j];
				}
				this.text.text = showString;

			}
			i += charactersPerFrame;
			yield return null;
		}

		isShowing = true;
	} 


}

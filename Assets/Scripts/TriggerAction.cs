﻿using UnityEngine;
using System.Collections;

public class TriggerAction : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetButtonDown("Fire1")) {

			RaycastHit hit;
			if (Physics.Raycast(transform.position, transform.forward, out hit)) {
				Debug.Log("Hit: "+hit.collider.name);

				NPC_Dialogue npc_d = hit.collider.GetComponent<NPC_Dialogue>();

				if (npc_d != null) {
					npc_d.StartDialogue();
				}
			}

		}


	}
}
